let renderDSSV = (dssv) => {
  let contentHTML = "";
  for (let i = 0; i < dssv.length; i++) {
    let contentTr = `<tr>
            <td>${dssv[i].ma}</td>
            <td>${dssv[i].ten}</td>
            <td>${dssv[i].email}</td>
            <td>0</td>
            <td>
                <button onclick="xoaSV(${dssv[i].ma})" class='btn btn-danger'>Xóa</button>
                <button onclick="suaSV(${dssv[i].ma})" class ='btn btn-warning'>Sửa</button>
            </td>
        </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

let layThongTinTuForm = () => {
  let ma = document.getElementById("txtMaSV").value;
  let ten = document.getElementById("txtTenSV").value;
  let email = document.getElementById("txtEmail").value;
  let matKhau = document.getElementById("txtPass").value;
  let toan = document.getElementById("txtDiemToan").value;
  let ly = document.getElementById("txtDiemLy").value;
  let hoa = document.getElementById("txtDiemHoa").value;

  return {
    ma,
    ten,
    email,
    matKhau,
    toan,
    ly,
    hoa,
  };
};

let showThongTinLenForm = (sv) => {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
};
let batLoading=()=>{
  document.getElementById('loading').style.display='flex'
}
let tatLoading=()=>{
  document.getElementById('loading').style.display='none'
}