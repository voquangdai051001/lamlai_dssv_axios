const BASE_URL = "https://63f442db864fb1d60024f96a.mockapi.io";

let feastDSSV = () => {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      let dssv = res.data;
      renderDSSV(dssv);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
feastDSSV();

let themSV = () => {
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then((res) => {
      feastDSSV();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

let xoaSV = (id) => {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      feastDSSV();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

let suaSV = (id) => {
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then((res) => {
      showThongTinLenForm(res.data);
      console.log(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

let capNhatSV = (id) => {
  let sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then((res) => {     
      feastDSSV();
      resetForm()
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

let resetForm = () => {
  document.getElementById("formQLSV").reset();
};
